# ACNL Monthly Overview

This document is a simple overview of each month in Animal Crossing: New Leaf. It includes monthly events and fish, bugs, and deep-sea creatures that go away after each month.

All information here is for the North America version of the game, and all prices do not include the bonus from the Bell Boom ordinance. If you have the Bell Boom ordinance, multiply the prices by 1.2.

This document is still work-in-progress, and will eventually have all of the following:

- [x] Events
- [x] Fish
- [ ] Bugs
- [ ] Deep-sea creatures

This was written in Markdown and converted to HTML, so it should load significantly quicker than other websites.

The majority of information and media here is from the [Animal Crossing Wiki](https://animalcrossing.fandom.com/wiki/Animal_Crossing_Wiki). Other sources include [Thonky](https://www.thonky.com/animal-crossing-new-leaf/), [Nookipedia](https://nookipedia.com/wiki/Main_Page), and the [Animal Crossing New Leaf Wiki](https://acnewleaf.fandom.com/wiki/Animal_Crossing_New_Leaf_Wiki).

Created by [Luke](https://twitter.com/ConsoleLogLuke). The [original Markdown](https://gitlab.com/devluke/animalcrossing/-/raw/master/original.md) (and the [converted HTML](https://gitlab.com/devluke/animalcrossing/-/raw/master/index.html)) can be found on [GitLab](https://gitlab.com/devluke/animalcrossing).

## January

### Events

| Name           | Date      | Time         | Start Location |
| -------------- | --------- | ------------ | -------------- |
| New Year's Day | January 1 | 6 AM - 12 AM | Plaza          |

### Fish

| Name | Image | Price | Location | Island? | Shadow Size | Time |
| ---- | ----- | ----- | -------- | ------- | ----------- | ---- |

## February

### Events

| Name                     | Date                  | Time    | Start Location |
| ------------------------ | --------------------- | ------- | -------------- |
| [Groundhog Day][ev-gd]   | February 2            | All day | Plaza          |
| [Valentine's Day][ev-vd] | February 14           | None    | None           |
| [Festivale][ev-fv]       | Day before Mardi Gras | All day | Plaza          |

### Fish

| Name                | Image                 | Price  | Location | Island? | Shadow Size | Time        |
| ------------------- | --------------------- | ------ | -------- | ------- | ----------- | ----------- |
| [Bitterling][bl]    | ![Bitterling][bli]    | 900    | River    | No      | 1           | All day     |
| [Pond smelt][ps]    | ![Pond smelt][psi]    | 300    | River    | No      | 2           | All day     |
| [Stringfish][sf]    | ![Stringfish][sfi]    | 15,000 | River    | No      | 6           | 4 PM - 9 AM |
| [Sea butterfly][sb] | ![Sea butterfly][sbi] | 1,000  | Ocean    | No      | 1           | All day     |
| [Blowfish][bf]      | ![Blowfish][bfi]      | 125    | Ocean    | No      | 3           | 9 PM - 4 AM |

## March

### Events

| Name                  | Date                  | Time    | Start Location |
| --------------------- | --------------------- | ------- | -------------- |
| [Festivale][ev-fv]    | Day before Mardi Gras | All day | Plaza          |
| [Shamrock Day][ev-sd] | March 17              | All day | Plaza          |
| [Bunny Day][ev-bd]    | Easter Sunday         | All day | Plaza          |

### Fish

| Name                | Image                 | Price | Location | Island? | Shadow Size | Time        |
| ------------------- | --------------------- | ----- | -------- | ------- | ----------- | ----------- |
| [Yellow perch][yp]  | ![Yellow perch][ypi]  | 240   | River    | No      | 3           | All day     |
| [Football fish][ff] | ![Football fish][ffi] | 2,500 | Ocean    | No      | 4           | 4 PM - 9 AM |
| [Tuna][tu]          | ![Tuna][tui]          | 7,000 | Ocean    | No      | 6           | All day     |

## April

### Events

| Name                             | Date                   | Time    | Start Location   |
| -------------------------------- | ---------------------- | ------- | ---------------- |
| [Bunny Day][ev-bd]               | Easter Sunday          | All day | Plaza            |
| [April Fool's Day][ev-af]        | April 1                | All day | In or near plaza |
| [Cherry Blossom Festival][ev-cb] | April 1 - 10           | All day | All over town    |
| [Nature Day][ev-nd]              | April 22               | All day | Plaza            |
| [Weeding Day][ev-wd]             | Last Saturday of April | All day | Plaza            |

### Fish

| Name      | Image       | Price | Location | Island? | Shadow Size | Time    |
| --------- | ----------- | ----- | -------- | ------- | ----------- | ------- |
| [Dab][da] | ![Dab][dai] | 300   | Ocean    | No      | 3           | All day |

## May

### Events

| Name                  | Date                 | Time | Start Location |
| --------------------- | -------------------- | ---- | -------------- |
| [Mother's Day][ev-md] | Second Sunday of May | None | None           |

### Fish

| Name          | Image           | Price | Location | Island? | Shadow Size | Time    |
| ------------- | --------------- | ----- | -------- | ------- | ----------- | ------- |
| [Loach][lo]   | ![Loach][loi]   | 300   | River    | No      | 2           | All day |
| [Oarfish][of] | ![Oarfish][ofi] | 9,000 | Ocean    | No      | 6           | All day |

## June

### Events

| Name                     | Date                 | Time    | Start Location |
| ------------------------ | -------------------- | ------- | -------------- |
| [Father's Day][ev-fd]    | Third Sunday of June | None    | None           |
| [Summer Solstice][ev-ss] | June 21              | All day | Plaza          |

### Fish

| Name                | Image                 | Price | Location  | Island? | Shadow Size | Time                       |
| ------------------- | --------------------- | ----- | --------- | ------- | ----------- | -------------------------- |
| [Cherry salmon][cs] | ![Cherry salmon][csi] | 1,000 | River     | No      | 3           | 4 AM - 9 AM<br>4 PM - 9 PM |
| [Char][ch]          | ![Char][chi]          | 3,800 | Waterfall | No      | 3           | 4 AM - 9 AM<br>4 PM - 9 PM |
| [Rainbow trout][rt] | ![Rainbow trout][rti] | 800   | River     | No      | 4           | 4 AM - 9 AM<br>4 PM - 9 PM |

## July

### Events

| Name | Date | Time | Start Location |
| ---- | ---- | ---- | -------------- |

### Fish

| Name          | Image           | Price | Location | Island? | Shadow Size | Time    |
| ------------- | --------------- | ----- | -------- | ------- | ----------- | ------- |
| [Tadpole][tp] | ![Tadpole][tpi] | 100   | Pond     | No      | 1           | All day |

## August

### Events

| Name                    | Date              | Time         | Start Location |
| ----------------------- | ----------------- | ------------ | -------------- |
| [Fireworks Show][ev-fs] | Sundays in August | 7 PM - 12 AM | Plaza          |

### Fish

| Name                  | Image                   | Price  | Location | Island? | Shadow Size | Time        |
| --------------------- | ----------------------- | ------ | -------- | ------- | ----------- | ----------- |
| [Killifish][kf]       | ![Killifish][kfi]       | 300    | Pond     | No      | 1           | All day     |
| [Frog][fr]            | ![Frog][fri]            | 120    | Pond     | No      | 2           | All day     |
| [Giant snakehead][gs] | ![Giant snakehead][gsi] | 5,500  | Lake     | No      | 5           | 9 AM - 4 PM |
| [Napoleonfish][nf]    | ![Napoleonfish][nfi]    | 10,000 | Ocean    | Yes     | 6           | 4 AM - 9 PM |
| [Squid][sq]           | ![Squid][sqi]           | 400    | Ocean    | Yes     | 3           | All day     |

## September

### Events

| Name                           | Date                                  | Time         | Start Location |
| ------------------------------ | ------------------------------------- | ------------ | -------------- |
| [Labor Day][ev-ld]             | First Monday of September             | All day      | Plaza          |
| [Harvest Moon Festival][ev-hm] | Full moon closest to Autumnal Equinox | 6 AM - 12 AM | Plaza          |

### Fish

| Name                      | Image                       | Price  | Location  | Island? | Shadow Size | Time                       |
| ------------------------- | --------------------------- | ------ | --------- | ------- | ----------- | -------------------------- |
| [Crawfish][cf]            | ![Crawfish][cfi]            | 200    | Pond      | No      | 2           | All day                    |
| [Soft-shelled turtle][ss] | ![Soft-shelled turtle][ssi] | 3,750  | River     | No      | 3           | 4 PM - 9 AM                |
| [Eel][ee]                 | ![Eel][eei]                 | 2,000  | River     | No      | Narrow      | 4 PM - 9 AM                |
| [Sweetfish][sw]           | ![Sweetfish][swi]           | 900    | River     | No      | 3           | All day                    |
| [Salmon][sa]              | ![Salmon][sai]              | 700    | River     | No      | 4           | All day                    |
| [King salmon][ks]         | ![King salmon][ksi]         | 1,800  | Waterfall | No      | 6           | All day                    |
| [Nibble fish][ni]         | ![Nibble fish][nii]         | 1,500  | River     | No      | 2           | 9 AM - 4 PM                |
| [Piranha][pi]             | ![Piranha][pii]             | 2,500  | River     | No      | 2           | 9 AM - 4 PM<br>9 PM - 4 AM |
| [Arowana][ar]             | ![Arowana][ari]             | 10,000 | River     | No      | 4           | 4 PM - 9 AM                |
| [Dorado][do]              | ![Dorado][doi]              | 15,000 | River     | No      | 5           | 4 AM - 9 PM                |
| [Gar][ga]                 | ![Gar][gai]                 | 6,000  | Lake      | No      | 6           | 4 PM - 9 AM                |
| [Arapaima][aa]            | ![Arapaima][aai]            | 10,000 | River     | No      | 6           | 4 PM - 9 AM                |
| [Saddled bichir][sd]      | ![Saddled bichir][sdi]      | 4,000  | River     | No      | 4           | 9 PM - 4 AM                |
| [Clownfish][cl]           | ![Clownfish][cli]           | 650    | Ocean     | Yes     | 1           | All day                    |
| [Surgeonfish][su]         | ![Surgeonfish][sui]         | 1,000  | Ocean     | Yes     | 2           | All day                    |
| [Butterfly fish][bu]      | ![Butterfly fish][bui]      | 1,000  | Ocean     | Yes     | 2           | All day                    |
| [Puffer fish][pf]         | ![Puffer fish][pfi]         | 240    | Ocean     | Yes     | 3           | All day                    |
| [Blue marlin][bm]         | ![Blue marlin][bmi]         | 10,000 | Ocean     | Yes     | 6           | All day                    |
| [Ocean sunfish][os]       | ![Ocean sunfish][osi]       | 4,000  | Ocean     | Yes     | 6 + fin     | 4 AM - 9 PM                |
| [Hammerhead shark][hs]    | ![Hammerhead shark][hsi]    | 8,000  | Ocean     | Yes     | 6 + fin     | 4 PM - 9 AM                |
| [Shark][sh]               | ![Shark][shi]               | 15,000 | Ocean     | Yes     | 6 + fin     | 4 PM - 9 AM                |
| [Saw shark][sr]           | ![Saw shark][sri]           | 12,000 | Ocean     | Yes     | 6 + fin     | 4 PM - 9 AM                |

## October

### Events

| Name                           | Date                                  | Time         | Start Location |
| ------------------------------ | ------------------------------------- | ------------ | -------------- |
| [Harvest Moon Festival][ev-hm] | Full moon closest to Autumnal Equinox | 6 AM - 12 AM | Plaza          |
| [Explorer's Day][ev-ed]        | Second Monday in October              | All day      | Plaza          |
| [Halloween][ev-hw]             | October 31                            | 6 PM - 1 AM  | All over town  |

### Fish

| Name            | Image             | Price | Location | Island? | Shadow Size | Time        |
| --------------- | ----------------- | ----- | -------- | ------- | ----------- | ----------- |
| [Catfish][ca]   | ![Catfish][cai]   | 800   | Lake     | No      | 4           | 4 PM - 9 AM |
| [Angelfish][af] | ![Angelfish][afi] | 3,000 | River    | No      | 2           | 4 PM - 9 AM |
| [Moray eel][me] | ![Moray eel][mei] | 2,000 | Ocean    | Yes     | 5           | All day     |

## November

### Events

| Name                        | Date                        | Time         | Start Location |
| --------------------------- | --------------------------- | ------------ | -------------- |
| [Mushrooming Season][ev-ms] | November 1 - 30             | All day      | All over town  |
| [Harvest Festival][ev-hf]   | Fourth Thursday in November | 6 AM - 12 AM | Plaza          |

### Fish

| Name                   | Image                    | Price | Location  | Island? | Shadow Size | Time                       |
| ---------------------- | ------------------------ | ----- | --------- | ------- | ----------- | -------------------------- |
| [Cherry salmon][cs]    | ![Cherry salmon][csi]    | 1,000 | River     | No      | 3           | 4 AM - 9 AM<br>4 PM - 9 PM |
| [Char][ch]             | ![Char][chi]             | 3,800 | Waterfall | No      | 3           | 4 AM - 9 AM<br>4 PM - 9 PM |
| [Rainbow trout][rt]    | ![Rainbow trout][rti]    | 800   | River     | No      | 4           | 4 AM - 9 AM<br>4 PM - 9 PM |
| [Mitten crab][mc]      | ![Mitten crab][mci]      | 2,000 | River     | No      | 2           | 4 PM - 9 AM                |
| [Guppy][gu]            | ![Guppy][gui]            | 1,300 | River     | No      | 1           | 9 AM - 4 PM                |
| [Neon tetra][nt]       | ![Neon tetra][nti]       | 500   | River     | No      | 1           | 9 AM - 4 PM                |
| [Seahorse][se]         | ![Seahorse][sei]         | 1,100 | Ocean     | Yes     | 1           | All day                    |
| [Zebra turkeyfish][zt] | ![Zebra turkeyfish][zti] | 400   | Ocean     | Yes     | 3           | All day                    |
| [Barred knifejaw][bk]  | ![Barred knifejaw][bki]  | 5,000 | Ocean     | Yes     | 3           | All day                    |
| [Ray][ra]              | ![Ray][rai]              | 3,000 | Ocean     | Yes     | 5           | 4 AM - 9 PM                |

## December

### Events

| Name                     | Date        | Time        | Start Location |
| ------------------------ | ----------- | ----------- | -------------- |
| [Winter Solstice][ev-ws] | December 21 | All day     | Plaza          |
| [Toy Day][ev-td]         | December 24 | 6 PM - 6 AM | All over town  |
| [New Year's Eve][ev-ny]  | December 31 | All day     | Plaza          |

### Fish

| Name       | Image        | Price | Location | Island? | Shadow Size | Time        |
| ---------- | ------------ | ----- | -------- | ------- | ----------- | ----------- |
| [Pike][pk] | ![Pike][pki] | 1,800 | River    | No      | 5           | 4 AM - 9 PM |

[//]: # (Event links)

[ev-gd]: https://animalcrossing.fandom.com/wiki/Groundhog_Day
[ev-vd]: https://animalcrossing.fandom.com/wiki/Valentine%27s_Day
[ev-fv]: https://animalcrossing.fandom.com/wiki/Festivale
[ev-sd]: https://animalcrossing.fandom.com/wiki/Shamrock_Day
[ev-bd]: https://animalcrossing.fandom.com/wiki/Bunny_Day
[ev-af]: https://animalcrossing.fandom.com/wiki/April_Fool%27s_Day
[ev-cb]: https://animalcrossing.fandom.com/wiki/Cherry_Blossom_Festival
[ev-nd]: https://animalcrossing.fandom.com/wiki/Nature_Day
[ev-wd]: https://animalcrossing.fandom.com/wiki/Weeding_Day
[ev-md]: https://animalcrossing.fandom.com/wiki/Mother%27s_Day
[ev-fd]: https://animalcrossing.fandom.com/wiki/Father%27s_Day
[ev-ss]: https://animalcrossing.fandom.com/wiki/Summer_Solstice
[ev-fs]: https://animalcrossing.fandom.com/wiki/Fireworks_Show
[ev-ld]: https://animalcrossing.fandom.com/wiki/Labor_Day
[ev-hm]: https://animalcrossing.fandom.com/wiki/Harvest_Moon_Festival
[ev-ed]: https://animalcrossing.fandom.com/wiki/Explorer%27s_Day
[ev-hw]: https://animalcrossing.fandom.com/wiki/Halloween
[ev-ms]: https://animalcrossing.fandom.com/wiki/Mushrooming_Season
[ev-hf]: https://animalcrossing.fandom.com/wiki/Harvest_Festival
[ev-ws]: https://animalcrossing.fandom.com/wiki/Winter_Solstice
[ev-td]: https://animalcrossing.fandom.com/wiki/Toy_Day
[ev-ny]: https://animalcrossing.fandom.com/wiki/New_Year%27s_Eve

[//]: # (Fish links)

[bl]: https://animalcrossing.fandom.com/wiki/Bitterling
[bli]: https://vignette.wikia.nocookie.net/animalcrossing/images/4/4a/Bitterling_HHD_Icon.png/revision/latest?cb=20161105193556
[ps]: https://animalcrossing.fandom.com/wiki/Pond_smelt
[psi]: https://vignette.wikia.nocookie.net/animalcrossing/images/c/cf/Pond_Smelt_HHD_Icon.png/revision/latest?cb=20161105203259
[sf]: https://animalcrossing.fandom.com/wiki/Stringfish
[sfi]: https://vignette.wikia.nocookie.net/animalcrossing/images/5/53/Stringfish_HHD_Icon.png/revision/latest?cb=20161105204949
[sb]: https://animalcrossing.fandom.com/wiki/Sea_butterfly
[sbi]: https://vignette.wikia.nocookie.net/animalcrossing/images/d/d6/Sea_Butterfly_HHD_Icon.png/revision/latest?cb=20161105204224
[bf]: https://animalcrossing.fandom.com/wiki/Blowfish
[bfi]: https://vignette.wikia.nocookie.net/animalcrossing/images/6/63/Blowfish_HHD_Icon.png/revision/latest?cb=20161105193618
[yp]: https://animalcrossing.fandom.com/wiki/Yellow_perch
[ypi]: https://vignette.wikia.nocookie.net/animalcrossing/images/f/f0/Yellow_Perch_HHD_Icon.png/revision/latest?cb=20161105205721
[ff]: https://animalcrossing.fandom.com/wiki/Football_fish
[ffi]: https://vignette.wikia.nocookie.net/animalcrossing/images/6/67/Football_Fish_HHD_Icon.png/revision/latest?cb=20161105195415
[tu]: https://animalcrossing.fandom.com/wiki/Tuna
[tui]: https://vignette.wikia.nocookie.net/animalcrossing/images/1/17/Tuna_HHD_Icon.png/revision/latest?cb=20161105205357
[da]: https://animalcrossing.fandom.com/wiki/Dab
[dai]: https://vignette.wikia.nocookie.net/animalcrossing/images/2/26/Dab_HHD_Icon.png/revision/latest?cb=20161105194714
[lo]: https://animalcrossing.fandom.com/wiki/Loach
[loi]: https://vignette.wikia.nocookie.net/animalcrossing/images/2/2b/Loach_HHD_Icon.png/revision/latest?cb=20161105201526
[of]: https://animalcrossing.fandom.com/wiki/Oarfish
[ofi]: https://vignette.wikia.nocookie.net/animalcrossing/images/f/f5/Oarfish_HHD_Icon.png/revision/latest?cb=20161105202700
[cs]: https://animalcrossing.fandom.com/wiki/Cherry_salmon
[csi]: https://vignette.wikia.nocookie.net/animalcrossing/images/0/05/Cherry_Salmon_HHD_Icon.png/revision/latest?cb=20161105194343
[ch]: https://animalcrossing.fandom.com/wiki/Char
[chi]: https://vignette.wikia.nocookie.net/animalcrossing/images/2/2c/Char_HHD_Icon.png/revision/latest?cb=20161105194339
[rt]: https://animalcrossing.fandom.com/wiki/Rainbow_trout
[rti]: https://vignette.wikia.nocookie.net/animalcrossing/images/e/e9/Rainbow_Trout_HHD_Icon.png/revision/latest?cb=20161105203521
[tp]: https://animalcrossing.fandom.com/wiki/Tadpole
[tpi]: https://vignette.wikia.nocookie.net/animalcrossing/images/e/e7/Tadpole_HHD_Icon.png/revision/latest?cb=20161105205146
[kf]: https://animalcrossing.fandom.com/wiki/Killifish
[kfi]: https://vignette.wikia.nocookie.net/animalcrossing/images/3/3f/Killifish_HHD_Icon.png/revision/latest?cb=20161105201157
[fr]: https://animalcrossing.fandom.com/wiki/Frog
[fri]: https://vignette.wikia.nocookie.net/animalcrossing/images/8/8f/Frog_HHD_Icon.png/revision/latest?cb=20161105195429
[gs]: https://animalcrossing.fandom.com/wiki/Giant_snakehead
[gsi]: https://vignette.wikia.nocookie.net/animalcrossing/images/4/4f/Giant_Snakehead_HHD_Icon.png/revision/latest?cb=20161105195626
[nf]: https://animalcrossing.fandom.com/wiki/Napoleonfish
[nfi]: https://vignette.wikia.nocookie.net/animalcrossing/images/d/d0/Napoleonfish_HHD_Icon.png/revision/latest?cb=20161105202558
[sq]: https://animalcrossing.fandom.com/wiki/Squid
[sqi]: https://vignette.wikia.nocookie.net/animalcrossing/images/a/a3/Squid_HHD_Icon.png/revision/latest?cb=20161105204858
[cf]: https://animalcrossing.fandom.com/wiki/Crawfish
[cfi]: https://vignette.wikia.nocookie.net/animalcrossing/images/e/e2/Crawfish_HHD_Icon.png/revision/latest?cb=20161105194621
[ss]: https://animalcrossing.fandom.com/wiki/Soft-shelled_turtle
[ssi]: https://vignette.wikia.nocookie.net/animalcrossing/images/a/a1/Soft-Shelled_Turtle_HHD_Icon.png/revision/latest?cb=20161105204721
[ee]: https://animalcrossing.fandom.com/wiki/Eel
[eei]: https://vignette.wikia.nocookie.net/animalcrossing/images/0/03/Eel_HHD_Icon.png/revision/latest?cb=20161105194855
[sw]: https://animalcrossing.fandom.com/wiki/Sweetfish
[swi]: https://vignette.wikia.nocookie.net/animalcrossing/images/5/5a/Sweetfish_HHD_Icon.png/revision/latest?cb=20161105205104
[sa]: https://animalcrossing.fandom.com/wiki/Salmon
[sai]: https://vignette.wikia.nocookie.net/animalcrossing/images/7/77/Salmon_HHD_Icon.png/revision/latest?cb=20161105204055
[ks]: https://animalcrossing.fandom.com/wiki/King_salmon
[ksi]: https://vignette.wikia.nocookie.net/animalcrossing/images/9/92/King_Salmon_HHD_Icon.png/revision/latest?cb=20161105201206
[ni]: https://animalcrossing.fandom.com/wiki/Nibble_fish
[nii]: https://vignette.wikia.nocookie.net/animalcrossing/images/e/e2/Nibble_Fish_HHD_Icon.png/revision/latest?cb=20161105202635
[pi]: https://animalcrossing.fandom.com/wiki/Piranha
[pii]: https://vignette.wikia.nocookie.net/animalcrossing/images/5/54/Piranha_HHD_Icon.png/revision/latest?cb=20161105203149
[ar]: https://animalcrossing.fandom.com/wiki/Arowana
[ari]: https://vignette.wikia.nocookie.net/animalcrossing/images/b/be/Arowana_HHD_Icon.png/revision/latest?cb=20161105192205
[do]: https://animalcrossing.fandom.com/wiki/Dorado
[doi]: https://vignette.wikia.nocookie.net/animalcrossing/images/f/ff/Dorado_HHD_Icon.png/revision/latest?cb=20161105194823
[ga]: https://animalcrossing.fandom.com/wiki/Gar
[gai]: https://vignette.wikia.nocookie.net/animalcrossing/images/5/54/Gar_HHD_Icon.png/revision/latest?cb=20161105195557
[aa]: https://animalcrossing.fandom.com/wiki/Arapaima
[aai]: https://vignette.wikia.nocookie.net/animalcrossing/images/7/79/Arapaima_HHD_Icon.png/revision/latest?cb=20161105192118
[sd]: https://animalcrossing.fandom.com/wiki/Saddled_bichir
[sdi]: https://vignette.wikia.nocookie.net/animalcrossing/images/b/b7/Saddled_Bichir_HHD_Icon.png/revision/latest?cb=20161105204041
[cl]: https://animalcrossing.fandom.com/wiki/Clownfish
[cli]: https://vignette.wikia.nocookie.net/animalcrossing/images/9/98/Clownfish_HHD_Icon.png/revision/latest?cb=20161105194432
[su]: https://animalcrossing.fandom.com/wiki/Surgeonfish
[sui]: https://vignette.wikia.nocookie.net/animalcrossing/images/8/83/Surgeonfish_HHD_Icon.png/revision/latest?cb=20161105205054
[bu]: https://animalcrossing.fandom.com/wiki/Butterfly_fish
[bui]: https://vignette.wikia.nocookie.net/animalcrossing/images/0/08/Butterflyfish_HHD_Icon.png/revision/latest?cb=20161105193824
[pf]: https://animalcrossing.fandom.com/wiki/Puffer_fish
[pfi]: https://vignette.wikia.nocookie.net/animalcrossing/images/e/e0/Puffer_Fish_HHD_Icon.png/revision/latest?cb=20161105203430
[bm]: https://animalcrossing.fandom.com/wiki/Blue_marlin
[bmi]: https://vignette.wikia.nocookie.net/animalcrossing/images/4/42/Blue_Marlin_HHD_Icon.png/revision/latest?cb=20161105193638
[os]: https://animalcrossing.fandom.com/wiki/Ocean_sunfish
[osi]: https://vignette.wikia.nocookie.net/animalcrossing/images/b/be/Ocean_Sunfish_HHD_Icon.png/revision/latest?cb=20161105202704
[hs]: https://animalcrossing.fandom.com/wiki/Hammerhead_shark
[hsi]: https://vignette.wikia.nocookie.net/animalcrossing/images/f/fb/Hammerhead_Shark_HHD_Icon.png/revision/latest?cb=20161105200656
[sh]: https://animalcrossing.fandom.com/wiki/Shark
[shi]: https://vignette.wikia.nocookie.net/animalcrossing/images/5/5b/Shark_HHD_Icon.png/revision/latest?cb=20161105204315
[sr]: https://animalcrossing.fandom.com/wiki/Saw_shark
[sri]: https://vignette.wikia.nocookie.net/animalcrossing/images/0/09/Saw_Shark_HHD_Icon.png/revision/latest?cb=20161105204119
[ca]: https://animalcrossing.fandom.com/wiki/Catfish
[cai]: https://vignette.wikia.nocookie.net/animalcrossing/images/4/41/Catfish_HHD_Icon.png/revision/latest?cb=20161105194321
[af]: https://animalcrossing.fandom.com/wiki/Angelfish
[afi]: https://vignette.wikia.nocookie.net/animalcrossing/images/4/40/Angelfish_HHD_Icon.png/revision/latest?cb=20161105192032
[me]: https://animalcrossing.fandom.com/wiki/Moray_eel
[mei]: https://vignette.wikia.nocookie.net/animalcrossing/images/7/78/Moray_Eel_HHD_Icon.png/revision/latest?cb=20161105202356
[mc]: https://animalcrossing.fandom.com/wiki/Mitten_crab
[mci]: https://vignette.wikia.nocookie.net/animalcrossing/images/5/59/Mitten_Crab_HHD_Icon.png/revision/latest?cb=20161105202117
[gu]: https://animalcrossing.fandom.com/wiki/Guppy
[gui]: https://vignette.wikia.nocookie.net/animalcrossing/images/3/34/Guppy_HHD_Icon.png/revision/latest?cb=20161105200652
[nt]: https://animalcrossing.fandom.com/wiki/Neon_tetra
[nti]: https://vignette.wikia.nocookie.net/animalcrossing/images/3/37/Neon_Tetra_HHD_Icon.png/revision/latest?cb=20161105202609
[se]: https://animalcrossing.fandom.com/wiki/Seahorse
[sei]: https://vignette.wikia.nocookie.net/animalcrossing/images/a/aa/Sea_Horse_HHD_Icon.png/revision/latest?cb=20161105204235
[zt]: https://animalcrossing.fandom.com/wiki/Zebra_turkeyfish
[zti]: https://vignette.wikia.nocookie.net/animalcrossing/images/3/3d/Zebra_Turkeyfish_HHD_Icon.png/revision/latest?cb=20161105205734
[bk]: https://animalcrossing.fandom.com/wiki/Barred_knifejaw
[bki]: https://vignette.wikia.nocookie.net/animalcrossing/images/5/52/Barred_Knifejaw_HHD_Icon.png/revision/latest?cb=20161105193452
[ra]: https://animalcrossing.fandom.com/wiki/Ray
[rai]: https://vignette.wikia.nocookie.net/animalcrossing/images/b/b9/Ray_HHD_Icon.png/revision/latest?cb=20161105203617
[pk]: https://animalcrossing.fandom.com/wiki/Pike
[pki]: https://vignette.wikia.nocookie.net/animalcrossing/images/a/ab/Pike_HHD_Icon.png/revision/latest?cb=20161105203053
